create or replace package fancytree_region_plugin
as
  function render_region(
    p_region in apex_plugin.t_region
    , p_plugin in apex_plugin.t_plugin
    , p_is_printer_friendly in boolean
  ) return apex_plugin.t_region_render_result;

  function ajax_region (
    p_region in apex_plugin.t_region
    , p_plugin in apex_plugin.t_plugin
  )return apex_plugin.t_region_ajax_result;
end fancytree_region_plugin;
/