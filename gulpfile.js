var gulp = require('gulp');
var zip = require('gulp-zip');
var clean = require('gulp-clean');

gulp.task('default', ['build'], function () {

});

gulp.task('dist', ['build'],
  function () {
    console.log("dist");
    return gulp.src('build/**/*')
      .pipe(zip('fancytree-region-plugin.zip'))
      .pipe(gulp.dest('dist'));
  });

gulp.task('build', ['clean'], function () {
  var one = gulp.src('./src/main/js/**/*.js')
    .pipe(gulp.dest('./build/js/'));
  var two = gulp.src('./node_modules/jquery.fancytree/dist/**/*')
    .pipe(gulp.dest('./build/lib/fancytree'));
  return one && two;
});

gulp.task('clean', function () {
  console.log('Clean build directory.');
  var one = gulp.src('build').pipe(clean());
  var two = gulp.src('dist').pipe(clean());
  return one && two;
});