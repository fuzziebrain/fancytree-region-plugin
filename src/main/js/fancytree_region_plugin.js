"use strict";

/**
 * Initialize fancytree
 */
function initTree(pRegionId, pAjaxId) {
  $(pRegionId).fancytree({
    source: getData(0, ''),
    toggleEffect: false,
    autoScroll: true,

    activate: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_activate",
        data);
    },

    blur: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_blur",
        data);
    },

    blurTree: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_blurtree",
        data);
    },

    click: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_click",
        data);
    },

    collapse: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_collapse",
        data);
    },

    dblclick: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_dblclick",
        data);
    },

    deactivate: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_deactivate",
        data);
    },

    expand: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_expand",
        data);
    },

    focus: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_focus",
        data);
    },

    focusTree: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_focustree",
        data);
    },

    keydown: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_keydown",
        data);
    },

    keypress: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_keypress",
        data);
    },

    select: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_select",
        data);
    },

    init: function (event, data) {
      apex.event.trigger(
        $x(pRegionId),
        "fancytree_init",
        data);
    },

    lazyLoad: function (event, data) {
      data.result = getData(data.node.getLevel(), data.node.key);
    }
  });

  /**
   * Function that loads the data for tree nodes.
   */
  function getData(level, parent_id) {
    var result;
    result = apex.server.plugin(
      pAjaxId, {
        "x01": level,
        "x02": parent_id
      }, {
        success: function (pData) {
          console.log("Successful AJAX call");
          return pData;
        },
        error: function (d) {
          console.log(d);
        },
        dataType: "json"
      }
    );
    console.log("Leaving...");
    return result;
  }
}