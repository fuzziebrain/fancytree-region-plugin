create or replace package body fancytree_region_plugin
as
  function render_region(
    p_region in apex_plugin.t_region
    , p_plugin in apex_plugin.t_plugin
    , p_is_printer_friendly in boolean
  ) return apex_plugin.t_region_render_result
  as
    l_region_id varchar2(100) ;
    l_return apex_plugin.t_region_render_result;
  begin
    -- TODO: Implementation required for FUNCTION
    -- FANCY_TREE_REGION_PLUGIN.render_region
    l_region_id := coalesce (p_region.static_id, to_char (p_region.id)) ;

    if apex_application.g_debug then
      apex_plugin_util.debug_region (p_plugin, p_region) ;
      apex_debug.message ('ajax identifier: ' ||apex_plugin.get_ajax_identifier
      ) ;
    end if;

    -- Add fancytree JS
    apex_javascript.add_library(
      p_name => 'jquery.fancytree-all'
      , p_directory => p_plugin.file_prefix||'lib/fancytree/'
      , p_version => null
      , p_check_to_add_minified => true
    );

    -- Add fancytree_region_plugin JS
    apex_javascript.add_library(
      p_name => 'fancytree_region_plugin'
      , p_directory => p_plugin.file_prefix||'js/'
      , p_version => null
      , p_check_to_add_minified => false
    );

    -- Add fancytree CSS
    apex_css.add_file(
      p_name => 'skin-win7'||'/ui.fancytree'
      , p_directory => p_plugin.file_prefix ||'lib/fancytree/'
      , p_version => null
    );

    -- Add onload code
    apex_javascript.add_onload_code(
      'inittree(fancytree_' || l_region_id || ', "'
      || apex_plugin.get_ajax_identifier ||'");'
    );
    sys.htp.prn ('<div id="fancytree_' ||l_region_id||'">');
    sys.htp.prn ('</div>');

    return l_return;
  end render_region;

  function ajax_region(
    p_region in apex_plugin.t_region
    , p_plugin in apex_plugin.t_plugin
  ) return apex_plugin.t_region_ajax_result
  as
    l_query varchar2(32767);
    l_sql2 varchar2(32767);
    l_level number;
    l_parent_id varchar2(32767);
    l_column_value_list apex_plugin_util.t_column_value_list;
    l_firstrow boolean := true ;
    l_return apex_plugin.t_region_ajax_result;
  begin
    l_level := to_number(coalesce(apex_application.g_x01, 0));
    if apex_application.g_x02 is not null then
      l_parent_id := sys.htf.escape_sc(to_char(apex_application.g_x02));
    end if;

    apex_debug.message('x01: '||l_level);
    apex_debug.message('x02: '||l_parent_id);

    l_query := 'select key, title, isleaf, icon, link, tooltip, data  from (' ||
        p_region.source || ')';
    if l_parent_id is null then
      l_query := l_query || 'where parent_id is null';
    else
      l_query := l_query || 'where parent_id = ''' || l_parent_id || '''';
    end if;

    apex_debug.message(l_query);

    /*
    Column (1): key
    Column (2): title
    Column (3): isleaf
    Column (4): icon
    Column (5): link/href
    Column (6): tooltip
    Column (7): data (object)
    */
    l_column_value_list := apex_plugin_util.get_data(
      p_sql_statement => l_query
      , p_min_columns => 7
      , p_max_columns => 7
      , p_component_name => null
    );

    apex_plugin_util.print_json_http_header;

    sys.htp.p('[') ;
    for
      i in 1..l_column_value_list(1).count
    loop
      if l_firstrow then
        l_firstrow := false ;
        sys.htp.p('{') ;
      else
        sys.htp.p(', {') ;
      end if ;

      sys.htp.p(apex_javascript.add_attribute('key', to_char (l_column_value_list(1)(i))));
      sys.htp.p(apex_javascript.add_attribute('title', to_char (l_column_value_list(2)(i))));
      if (to_number(l_column_value_list(3)(i)) = 1) then
        sys.htp.p(apex_javascript.add_attribute('lazy', false));
      else
        sys.htp.p(apex_javascript.add_attribute('lazy', true));
      end if;
      sys.htp.p(apex_javascript.add_attribute('icon', sys.htf.escape_sc(l_column_value_list(4)(i))));
      sys.htp.p(apex_javascript.add_attribute('href', sys.htf.escape_sc(l_column_value_list(5)(i))));
      sys.htp.p(apex_javascript.add_attribute('tooltip', sys.htf.escape_sc(l_column_value_list(6)(i))));
      sys.htp.p(apex_javascript.add_attribute('data', sys.htf.escape_sc(l_column_value_list(7)(i)), false, false));
      sys.htp.p('}') ;
    end loop ;
    sys.htp.p(']') ;
    return l_return;
  end ajax_region;
end fancytree_region_plugin;
/